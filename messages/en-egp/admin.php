<?php

return [
    'motion_type_templ_appl'      => 'Standard template: candidature',
    'motion_type_templ_appsingle' => 'Candidature',
    'motion_type_templ_appplural' => 'Candidatures',
    'motion_type_templ_pdfappl'   => 'Standard template: PDF-candidature',

];
